#pragma once
#include "common.h"
#include "tensor.h"
#include "params.h"

class Layer{		
protected:
	Params* params;
	Tensor Xin;
public:		
	Mat_<T> W, dW;
	virtual void forward(Tensor& Xin, Tensor& Xout) = 0;
	virtual void backward(Tensor& dXin, Tensor& dXout) = 0;	
	void saveToFile(FILE* f);
};

class FullLayer : public Layer{	
	int depth_in, depth_out;
public:
	Mat_<T> Xin_pad;
	FullLayer(int d, int m);
	FullLayer(int d, int m, Params* params);
	void forward(Tensor& Xin, Tensor& Xout);
	void backward(Tensor& dXin, Tensor& dXout);
};

class Convolution : public Layer{
	int rows_in, cols_in, depth_in, rows_out, cols_out, depth_out;
	int w, pad, stride;	
	Mat_<T> Xin_conv;
public:
	Convolution(int depth_in, int rows_in, int w, int depth_out);
	Convolution(Params* params);
	void forward(Tensor& Xin, Tensor& Xout);
	void backward(Tensor& dXin, Tensor& dXout);
};

class ReLU : public Layer{
	T leak;
public:	
	ReLU();
	void forward(Tensor& Xin, Tensor& Xout);
	void backward(Tensor& dXin, Tensor& dXout);
};

class MaxPool : public Layer{
	int rows_in, cols_in, depth_in, rows_out, cols_out, depth_out;
	int size, stride;
	Tensor Maxi;
public:
	MaxPool(int rows_in, int cols_in, int depth_in, int size, int stride);
	void forward(Tensor& Xin, Tensor& Xout);
	void backward(Tensor& dXin, Tensor& dXout);
};

class Dropout : public Layer{
	T drop_prob, drop_prob_stash, iprob;
	Tensor mask;
public:
	Dropout(T prob);
	void trainingMode();
	void testMode();
	void forward(Tensor& Xin, Tensor& Xout);
	void backward(Tensor& dXin, Tensor& dXout);
};

class SoftMax : public Layer{
	void forward(Tensor& Xin, Tensor& Xout);
	void backward(Tensor& dXin, Tensor& dXout);
};