#pragma once
#include "opencv2\opencv.hpp"
#include <time.h>
#include <stdio.h>
#include <windows.h>
#include <string.h>
#include <random>

using namespace std;
using namespace cv;

typedef double T;

class FileGetter{
	WIN32_FIND_DATAA found;
	HANDLE hfind;
	char folder[256];
	int chk;
	bool first;
public:
	FileGetter(char* folderin, char* ext = 0);
	int getNextFile(char* fname);
	int getNextAbsFile(char* fname);
};

class Timer{
private:
	double startTime;
	char str[256];
public:
	Timer();
	void tic();
	void tic(char* str);
	double toc(int nrtimes = 1);
	void getTime(char* ttime);
};




T maxall(Mat_<T> mat);
int argmax_row(Mat_<T> mat, int row);
bool isInside(int row, int col, Mat_<T> img);
