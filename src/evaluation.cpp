#include "evaluation.h"

void Evaluation::showcase(Data* data, Network* nn){
	default_random_engine dre;
	uniform_int_distribution<int> dist(0, data->getNoInstances());

	Tensor Xout;
	nn->forward(data->X, Xout);
	data->X.tensorize();
	Mat_<T> Xout_lin = Xout.linearize();
	printf("\n");
	while (1){
		int row = dist(dre);
		imshow("input img", data->X[row][0]);
		printf("target: %2.5f predicted: ", data->y(row));
		int maxi = 0;
		for (int i = 1; i < Xout_lin.cols; i++)
		if (Xout_lin(row, i) > Xout_lin(row, maxi))
			maxi = i;
		printf("%d - scores:", maxi);
		for (int i = 0; i < Xout_lin.cols; i++)
			printf("%2.5f ", Xout_lin(row,i));
		printf("\n");
		waitKey();
	}
}

T Evaluation::classification_error(Data* data, Network* nn){
	const int n = data->getNoInstances();
	const int m = data->getNoClasses();
	Tensor Xout;
	Mat_<T> pred(n, m);
	int batch_size = 100;
	data->createBatches(batch_size, 0);
	int n2 = n / batch_size;
	if (n%batch_size)
		n2++;
	for (int i = 0; i < n2; i++){
		Data batch = data->nextBatch();
		nn->forward(batch.X, Xout);
		Mat_<T> Xout_lin = Xout.linearize();
		for (int ii = 0; ii < batch.getNoInstances(); ii++){
			for (int jj = 0; jj < m; jj++)
				pred(i*batch_size + ii, jj) = Xout_lin(ii, jj);
		}
	}
	T res = 0;
	for (int i = 0; i < n; i++)
	{
		res += argmax_row(pred, i) != data->y(i);
	}
	res /= n;
	printf("classification error %.4f %%\n", res*100);
	return res;
}