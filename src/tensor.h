#pragma once
#include "common.h"

enum form{
	TENSOR_FORM, MATRIX_FORM
};

class Tensor{
protected:
	int noInst, depth, height, width;
	vector<vector<Mat_<T> > > tform;
	Mat_<T> mform;
	form currentForm;
public:
	Tensor();	
	Tensor(int noInst, int depth, int height, int width);
	Tensor(int noInst, int depth);
	Tensor(const Tensor& o);
	//Tensor(Tensor&& o);
	Tensor copy();
	Tensor& operator = (Tensor& o);
	void create(int noInst, int depth, int height, int width);
	void create(int noInst, int depth);
	void createLike(Tensor& o);
	void copyDims(Tensor& o);
	int getNoInstances() const;	
	int getDepth() const;
	int getHeight() const;
	int getWidth() const;
	int getDim() const;
	form getCurrentForm() const{
		return currentForm;
	}

	Mat_<T>& linearize();
	void tensorize();

	vector<Mat_<T>>& operator [] (const int i){
		return tform[i];
	}	

	T& operator () (const int i, const int j){
		return mform(i, j);
	}

	void operator = (const Mat_<T>& o){
		mform = o;
		currentForm = MATRIX_FORM;
	}
};