#include "network.h"

Network::Network(Params* params = 0){
	this->params = params;
	outlayer = nullptr;
}

Network::~Network(){
	for (Layer* l : layers){
		if (l != nullptr)
			delete l;
	}
	if (outlayer!=nullptr)
		delete outlayer;
}

void Network::forward(Tensor& Xin, Tensor& Xout){
	if (layers.size() == 1)
		layers[0]->forward(Xin, Xout);
	else{
		Tensor* ts = new Tensor[layers.size() - 1];
		layers[0]->forward(Xin, ts[0]);
		for (int i = 1; i < layers.size() - 1; i++)
			layers[i]->forward(ts[i - 1], ts[i]);
		layers[layers.size() - 1]->forward(ts[layers.size() - 2], Xout);
		delete[] ts;
	}
}

void Network::forward(Tensor& Xin, Mat_<T>& Target, T& out){
	Tensor* ts = new Tensor[layers.size()];
	layers[0]->forward(Xin, ts[0]);
	for (int i = 1; i < layers.size(); i++)
		layers[i]->forward(ts[i - 1], ts[i]);
	outlayer->forward(ts[layers.size() - 1], Target, out);
	delete[] ts;
}

void Network::backward(Tensor& dXin, Mat_<T>& Target, T dout){
	Tensor* ts = new Tensor[layers.size()];
	outlayer->backward(ts[layers.size()-1], Target, dout);
	for (size_t i = layers.size() - 1; i > 0; i--)
		layers[i]->backward(ts[i-1], ts[i]);
	layers[0]->backward(dXin, ts[0]);
	delete[] ts;
	//remove dXin calculation for layers[0] - not needed
}

void Network::add(Layer *l){
	layers.push_back(l);
}

void Network::saveToFile(char* fname, Params* outparams){
	FILE* f = fopen(fname, "w");
	if (!f) return;
	outparams->saveToFile(f);	
	fprintf(f, "\n");
	params->saveToFile(f);
	fprintf(f, "\n");
	for (Layer* l : layers)
		l->saveToFile(f);
	fclose(f);
}