#pragma once
#include "common.h"
#include "outlayer.h"
#include "layer.h"

class Network{	
	Params* params;
public:	
	Network(Params* params);
	~Network();
	OutLayer* outlayer;
	vector<Layer*> layers;
	Network copy();
	void forward(Tensor& Xin, Tensor& Xout);
	void forward(Tensor& Xin, Mat_<T>& Target, T& out);
	void backward(Tensor& dXin, Mat_<T>& Target, T dout);
	void add(Layer *l);
	void saveToFile(char* fname, Params* outparams);
	void readFromFile(char* fname);
};
