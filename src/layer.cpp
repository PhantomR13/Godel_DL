#include "layer.h"
#undef max
#undef min

#define FIXED_SEED 100
//#define SHOW_TIMING 1
//optimizations: 0 - opencv functions; 1 - brute force unrolled for loops; 2 - openmp parallelized version
#define FULL_LAYER_OPT 2
#define RELU_LAYER_OPT 2
#define CONV_LAYER_OPT 2

void Layer::saveToFile(FILE* f){
	for (int i = 0; i < W.rows; i++){
		for (int j = 0; j < W.cols; j++)
			fprintf(f, "%3.7f ", W(i, j));
		fprintf(f, "\n");
	}
}

FullLayer::FullLayer(int depth_in, int depth_out){
	this->depth_in = depth_in;
	this->depth_out = depth_out;
	W = Mat_<T>(depth_in + 1, depth_out);
#if FIXED_SEED 
	long long seed = FIXED_SEED;
#else
	long long seed = clock();
#endif
	cout << "seed for layer " << seed << endl;
	cv::theRNG().state = seed;
	randn(W, 0, 2.0 / sqrt(depth_in + depth_out));
}

FullLayer::FullLayer(int d, int m, Params* params){
	W = Mat_<T>(d + 1, m);
	cv::theRNG().state = clock();
	randn(W, 0, params->get("full_layer_std_dev"));
}

void mulmat(Mat_<T>& A, Mat_<T>& B, Mat_<T>& res){
	if (res.rows == 0)
		res.create(A.rows, B.cols);
	if (A.cols != B.rows)
		throw std::exception("mulmat matrix dimensions incorrect");
	const int n = A.rows;
	const int m = B.cols;
	const int h = A.cols;
#pragma omp parallel for 
	for (int i = 0; i<n; i++){
		T* data = (T*)res.ptr(i);
		for (int j = 0; j<m; j++){
			T sum = 0;
			for (int k = 0; k<h; k++){
				sum += A(i, k)*B(k, j);
			}
			data[j] = sum;
		}
	}
}

void FullLayer::forward(Tensor& Xin, Tensor& Xout){
	Timer t; t.tic("full layer forward");
	Mat_<T> Xin_lin = Xin.linearize();
	this->Xin = Xin;
	Xout.create(Xin.getNoInstances(), depth_out);
	hconcat(Xin_lin, Mat_<T>::ones(Xin_lin.rows, 1), Xin_pad);
#if FULL_LAYER_OPT == 0
	Xout = Xin_pad*W;
#elif FULL_LAYER_OPT == 2
	Mat_<T> Xout_lin = Xout.linearize();
	mulmat(Xin_pad, W, Xout_lin);

#endif
#if SHOW_TIMING
	t.toc();
#endif
}

void FullLayer::backward(Tensor& dXin, Tensor& dXout){
	Timer t; t.tic("full layer backward");
	dXin.createLike(Xin);
	dXin.copyDims((Xin));
	Mat_<T> dXout_lin = dXout.linearize();
#if FULL_LAYER_OPT == 0
	dW = Xin_pad.t() * dXout_lin;	
	dXin = dXout_lin * W.rowRange(0, W.rows - 1).t();  //ignore last row corresponding to padding with 1	
#elif FULL_LAYER_OPT == 2
	Mat_<T> Xin_pad_t = Xin_pad.t();
	Mat_<T> W_t = W.rowRange(0, W.rows - 1).t();
	Mat_<T> dXin_lin = Xin.linearize();
	mulmat(Xin_pad_t, dXout_lin, dW);
	mulmat(dXout_lin, W_t, dXin_lin);
	dXin = dXin_lin;
#endif
#if SHOW_TIMING
	t.toc();
#endif
}

ReLU::ReLU(){
	leak = 0;// 1e-2;
}

void ReLU::forward(Tensor& Xin, Tensor& Xout){
	Timer t; t.tic("relu layer forward");
	this->Xin = Xin;
	Xout.createLike(Xin);
	/*
	Mat_<T> Xin_lin = Xin.linearize();
	Xout.copyShape(Xin);
	Xout.create(Xin_lin.rows, Xin_lin.cols);
	Mat_<T> Xout_lin = Xout.linearize();
	*/
#if RELU_LAYER_OPT == 0
	Xout = cv::max(Xin_lin, Mat_<T>::zeros(Xin_lin.size()));
#elif	RELU_LAYER_OPT == 1	
	for (int i = 0; i < Xout_lin.rows; i++){
		for (int j = 0; j < Xout_lin.cols; j++){
			if (Xin_lin(i, j) < 0)
				Xout_lin(i, j) = leak*Xin_lin(i, j);
			else
				Xout_lin(i, j) = Xin_lin(i, j);			
		}
	}	
#elif RELU_LAYER_OPT == 2
	if (Xin.getCurrentForm() == MATRIX_FORM){
#pragma omp parallel for 
		for (int i = 0; i < Xout.getNoInstances(); i++){
			T* datain = (T*)Xin.linearize().ptr(i);
			T* dataout = (T*)Xout.linearize().ptr(i);
			for (int j = 0; j < Xout.getDim(); j++){
				if (datain[j] > 0)
					dataout[j] = datain[j];
				else
					dataout[j] = leak*datain[j];
			}
		}
	}
	else{
//#pragma omp parallel for 
		for (int i = 0; i < Xin.getNoInstances(); i++){
			for (int d = 0; d < Xin.getDepth(); d++){
				for (int row = 0; row < Xin.getHeight(); row++){
					for (int col = 0; col < Xin.getWidth(); col++){
						if (Xin[i][d](row, col) > 0)
							Xout[i][d](row, col) = Xin[i][d](row, col);
						else
							Xout[i][d](row, col) = leak*Xin[i][d](row, col);
					}
				}
			}
		}
	}	
#endif
#if SHOW_TIMING
	t.toc();
#endif
}

void ReLU::backward(Tensor& dXin, Tensor& dXout){
	Timer t; t.tic("relu layer backward");
	dXin.createLike(dXout);
#if RELU_LAYER_OPT == 0
	//multiply((Xin_lin > 0), dXout_lin, dXin_lin);
#elif RELU_LAYER_OPT == 1
	Mat_<T> Xin_lin = Xin.linearize();	
	Mat_<T> dXout_lin = dXout.linearize();	
	Mat_<T> dXin_lin = dXin.linearize();	
	for (int i = 0; i < dXin_lin.rows; i++){
		for (int j = 0; j < dXin_lin.cols; j++){
			if (Xin_lin(i, j) > 0)
				dXin_lin(i, j) = dXout_lin(i, j);
			else
				dXin_lin(i, j) = leak*dXout_lin(i, j);
		}
	}	
#elif RELU_LAYER_OPT == 2
	if (dXout.getCurrentForm() == MATRIX_FORM){
		Mat_<T> Xin_lin = Xin.linearize();
		Mat_<T> dXout_lin = dXout.linearize();
		Mat_<T> dXin_lin = dXin.linearize();
#pragma omp parallel for 
		for (int i = 0; i < dXin_lin.rows; i++){
			T* datain = (T*)Xin_lin.ptr(i);
			T* datadin = (T*)dXin_lin.ptr(i);
			T* datadout = (T*)dXout_lin.ptr(i);
			for (int j = 0; j < dXin_lin.cols; j++){
				if (datain[j] > 0)
					datadin[j] = datadout[j];
				else
					datadin[j] = leak*datadout[j];
			}
		}
	}
	else{
		Xin.tensorize();
//#pragma omp parallel for 
		for (int i = 0; i < dXout.getNoInstances(); i++){
			for (int d = 0; d < dXout.getDepth(); d++){
				for (int row = 0; row < dXout.getHeight(); row++){
					for (int col = 0; col < dXout.getWidth(); col++){
						if (Xin[i][d](row, col) > 0)
							dXin[i][d](row, col) = dXout[i][d](row, col);
						else
							dXin[i][d](row, col) = leak*dXout[i][d](row, col);
					}
				}
			}
		}
	}	
#endif
#if SHOW_TIMING
	t.toc();
#endif
}

void SoftMax::forward(Tensor& Xin, Tensor& Xout){
	const T eps = 1e-2;
	this->Xin = Xin.copy();
	Xout = Xin.copy();
	Mat_<T> Xin_lin = Xin.linearize();
	Mat_<T> Xout_lin = Xout.linearize();
	for (int i = 0; i < Xin_lin.rows; i++){
		T mv = Xin_lin(0, 0);
		for (int j = 1; j < Xin_lin.cols; j++){
			if (Xin_lin(i, j) > mv)
				mv = Xin_lin(i, j);
		}
		T sum = 0;
		for (int j = 0; j < Xin_lin.cols; j++){
			Xout_lin(i, j) = exp(Xin_lin(i, j) - mv);
			sum += Xout_lin(i, j);
		}
		if (sum == 0){
			for (int j = 0; j < Xout_lin.cols; j++)
				Xout_lin(i, j) = 1.0/Xin_lin.cols;
		}
		else{
			for (int j = 0; j < Xout_lin.cols; j++){
				Xout_lin(i, j) /= sum;
				//Xout_lin(i, j) = max(eps, min(1 - eps, Xout_lin(i, j)));
			}
		}
			
	}
}

void SoftMax::backward(Tensor& dXin, Tensor& dXout){
	dXin = Xin.copy();
	Mat_<T> dXin_lin = dXin.linearize();
	Mat_<T> dXout_lin = dXout.linearize();

	for (int i = 0; i < dXout_lin.rows; i++){
		for (int j = 0; j < dXout_lin.cols; j++){
			dXin_lin(i, j) = 0;
			for (int k = 0; k < dXout_lin.cols; k++){
				if (j == k)
					dXin_lin(i, j) += dXout_lin(i, k)*(1 - dXout_lin(i, k));
				else
					dXin_lin(i, j) -= dXout_lin(i, k)*dXout_lin(i, k);
			}
		}
	}
}

Convolution::Convolution(int depth_in, int rows_in, int w, int depth_out){
	this->rows_in = rows_in;
	this->cols_in = rows_in;
	this->depth_in = depth_in;

	this->w = w;
	this->pad = w / 2;
	this->stride = 1;

	this->rows_out = (rows_in + 2 * pad - w) / stride + 1;
	this->cols_out = (cols_in + 2 * pad - w) / stride + 1;
	this->depth_out = depth_out;

	W.create((w*w + 1)*depth_in, depth_out); 
	dW.create((w*w + 1)*depth_in, depth_out);
#if FIXED_SEED 
	long long seed = FIXED_SEED;
#else
	long long seed = ((long long)(this)) * clock();
#endif
	cout << "seed for layer " << seed << endl;
	cv::theRNG().state = seed;
	//randn(W, 0, 2.0 / ((w*w + 1) * depth_in * depth_out));
	randn(W, 0, 1e-1);
	//randu(W, Scalar(-1e-4), Scalar(1e-4));
	for (int j = 0; j < W.cols; j++)
		W(W.rows - 1, j) = 0.000;
}

Convolution::Convolution(Params* params){
	rows_in = (int)params->get("rows_in");
	cols_in = (int)params->get("cols_in");
	depth_in = (int)params->get("depth_in");

	w = (int)params->get("w");
	pad = (int)params->get("pad");
	stride = (int)params->get("stride");

	rows_out = (rows_in + 2*pad - w) / stride + 1;
	cols_out = (cols_in + 2*pad - w) / stride + 1;
	depth_out = (int)params->get("depth_out");

	W.create((w*w + 1)*depth_in, depth_out); //drmem
	dW.create((w*w + 1)*depth_in, depth_out); //drmem
#if FIXED_SEED 
	long long seed = FIXED_SEED;
#else
	long long seed = clock();
#endif
	cout << "seed for layer " << seed << endl;
	cv::theRNG().state = seed;
	//randn(W, 0, 2.0 / ((w*w + 1) * depth_in * depth_out));
	randn(W, 0, 1e-2);
	for (int j = 0; j < W.cols; j++)
		W(W.rows - 1, j) = 0.000;
}

void Convolution::forward(Tensor& Xin, Tensor& Xout){
	Timer t; t.tic("convolution forward");
	Xin.tensorize();
	this->Xin = Xin;
	Xout.create(Xin.getNoInstances(), depth_out, rows_out, cols_out);
#if CONV_LAYER_OPT == 0
	//filter with opencv and add
	int w2 = w / 2;
	int w3 = w*w;
	int sz_in = rows_in * cols_in;
	const int noInst = Xin.getNoInstances();
	Tensor Ws(depth_in, depth_out, w, w);
	for (int d = 0; d < depth_in; d++){
		for (int d_out = 0; d_out < depth_out; d_out++){
			for (int u = 0; u < w; u++){
				for (int v = 0; v < w; v++){
					Ws[d][d_out](u, v) = W(d*w3 + u*w + v, d_out);
				}
			}
		}
	}
	for (int i = 0; i < noInst; i++){
		for (int d_out = 0; d_out < depth_out; d_out++){
			Xout[i][d_out].setTo(W(W.rows - 1, d_out)); 
			Mat_<T> tmp_conv_res(rows_out, cols_out);
			for (int d = 0; d < depth_in; d++){				
				if (sizeof(T) == 4)
					filter2D(Xin[i][d], Ws[d][d_out], CV_32FC1, tmp_conv_res, Point(-1, -1), 0, BORDER_CONSTANT);
				else
					filter2D(Xin[i][d], Ws[d][d_out], CV_64FC1, tmp_conv_res, Point(-1, -1), 0, BORDER_CONSTANT);
				Xout[i][d_out] += tmp_conv_res;
			}
		}
	}
#elif CONV_LAYER_OPT == 1
	int w2 = w / 2;
	int w3 = w*w;
	int sz_in = rows_in * cols_in;
	
	const int noInst = Xin.getNoInstances();
	for (int i = 0; i < noInst; i++){
		//vector<Mat_<T>> Xouti = Xout[i];
		for (int d_out = 0; d_out < depth_out; d_out++){
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					Xout[i][d_out](row, col) = W(W.rows - 1, d_out); //bias term, last element in row
					//Xouti[d_out](row, col) = W(W.rows - 1, d_out); //bias term, last element in row
					for (int d = 0; d < depth_in; d++){
						for (int u = 0; u < w; u++){
							for (int v = 0; v < w; v++){
								if (row + u - w2 >= 0 && col + v - w2 >= 0 && row + u - w2 < rows_in && col + v - w2 < cols_in)
									Xout[i][d_out](row, col) += Xin[i][d](row + u - w2, col + v - w2) * W(d*w3 + u*w + v, d_out);
									//Xouti[d_out](row, col) += Xin[i][d](row + u - w2, col + v - w2) * W(d*w3 + u*w + v, d_out);
							}
						}
					}
				}
			}
		}
	}
#elif CONV_LAYER_OPT == 2
	//openmp parallel
	int w2 = w / 2;
	int w3 = w*w;
	int sz_in = rows_in * cols_in;
	const int noInst = Xin.getNoInstances();
#pragma omp parallel for schedule(static)
	for (int i = 0; i < noInst; i++){
		vector<Mat_<T>>& Xouti = Xout[i];
		for (int d_out = 0; d_out < depth_out; d_out++){
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					Xouti[d_out](row, col) = W(W.rows - 1, d_out); //bias term, last element in row
					for (int d = 0; d < depth_in; d++){
						for (int u = 0; u < w; u++){
							for (int v = 0; v < w; v++){
								if (row + u - w2 >= 0 && col + v - w2 >= 0 && row + u - w2 < rows_in && col + v - w2 < cols_in)
								Xouti[d_out](row, col) += Xin[i][d](row + u - w2, col + v - w2) * W(d*w3 + u*w + v, d_out);
							}
						}
					}
				}
			}
		}
	}
#elif CONV_LAYER_OPT == 3
	
#endif
#if SHOW_TIMING
	t.toc();
#endif
}

void Convolution::backward(Tensor& dXin, Tensor& dXout){
	Timer t; t.tic("convolution backward");
	dW.setTo(0);
	dXin.create(dXout.getNoInstances(), depth_in, rows_in, cols_in);
	Xin.tensorize();
	dXout.tensorize();
#if CONV_LAYER_OPT == 0
	Mat_<T> dXout_conv(dXout.getNoInstances()*rows_out*cols_out, depth_out);
	int sz_out = rows_out * cols_out;
	for (int i = 0; i < Xin.getNoInstances(); i++){
		for (int row = 0; row < rows_out; row++){
			for (int col = 0; col < cols_out; col++){
				for (int d = 0; d < depth_out; d++){
					dXout_conv(i*sz_out + row*cols_out + col, d) = dXout[i][d](row, col);
				}
			}
		}
	}

	dW = Xin_conv.t() * dXout_conv;
	Mat_<T> dXin_conv = dXout_conv * W.t();

	int w2 = w / 2;
	int w3 = w*w + 1;
	int sz_in = rows_in * cols_in;
	for (int i = 0; i < Xin.getNoInstances(); i++){
		for (int d = 0; d < depth_in; d++){
			dXin[i][d].setTo(0);
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					for (int u = 0; u < w; u++){
						for (int v = 0; v < w; v++){
							if (isInside(row + u - w2, col + v - w2, dXin[0][0]))
								dXin[i][d](row + u - w2, col + v - w2) += dXin_conv(i*sz_in + row*cols_in + col, d*w3 + u*w + v);
						}
					}
				}
			}
		}
	}

#elif CONV_LAYER_OPT == 1
	int w2 = w / 2;
	int w3 = w*w;
	int sz_in = rows_in * cols_in;
	const int noInst = Xin.getNoInstances();



	for (int i = 0; i < noInst; i++){
		for (int d_out = 0; d_out < depth_out; d_out++){
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					dW(dW.rows - 1, d_out) += dXout[i][d_out](row, col);
					for (int d = 0; d < depth_in; d++){
						for (int u = 0; u < w; u++){
							for (int v = 0; v < w; v++){
								if (row + u - w2 >= 0 && col + v - w2 >= 0 && row + u - w2 < rows_in && col + v - w2 < cols_in){
									//Xouti[d_out](row, col) += Xin[i][d](row + u - w2, col + v - w2) * W(d*w3 + u*w + v, d_out);
									dW(d*w3 + u*w + v, d_out) += dXout[i][d_out](row, col) * Xin[i][d](row + u - w2, col + v - w2);
								}
							}
						}
					}
				}
			}
		}
	}

#elif CONV_LAYER_OPT == 2
	int w2 = w / 2;
	int w3 = w*w;
	int sz_in = rows_in * cols_in;
	const int noInst = Xin.getNoInstances();

	//backprop for weights paralellized on depth_out
#pragma omp parallel for schedule(static)
	for (int d_out = 0; d_out < depth_out; d_out++){
		Mat_<T> dWcol = Mat_<T>::zeros(dW.rows, 1);
		for (int i = 0; i < noInst; i++){
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					dWcol(W.rows - 1) += dXout[i][d_out](row, col);
					for (int d = 0; d < depth_in; d++){
						for (int u = 0; u < w; u++){
							for (int v = 0; v < w; v++){
								if (row + u - w2 >= 0 && col + v - w2 >= 0 && row + u - w2 < rows_in && col + v - w2 < cols_in){
									dWcol(d*w3 + u*w + v) += dXout[i][d_out](row, col) * Xin[i][d](row + u - w2, col + v - w2);
								}
							}
						}
					}
				}
			}
		}
		dW.col(d_out) += dWcol;
	}

	//backprop for inputs paralellized on instances
	for (int i = 0; i < noInst; i++){
		for (int d = 0; d < depth_in; d++){
			dXin[i][d].setTo(0);
		}
	}
	//#pragma omp parallel for schedule(static)
	for (int i = 0; i < noInst; i++){
		//vector<Mat_<T>> dXini = dXin[i];
		for (int d_out = 0; d_out < depth_out; d_out++){
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					for (int d = 0; d < depth_in; d++){
						for (int u = 0; u < w; u++){
							for (int v = 0; v < w; v++){
								if (row + u - w2 >= 0 && col + v - w2 >= 0 && row + u - w2 < rows_in && col + v - w2 < cols_in){
									//dXini[d](row + u - w2, col + v - w2) += dXout[i][d_out](row, col) * W(d*w3 + u*w + v, d_out);
									dXin[i][d](row + u - w2, col + v - w2) += dXout[i][d_out](row, col) * W(d*w3 + u*w + v, d_out);
								}
							}
						}
					}
				}
			}
		}
	}	
#endif
#if SHOW_TIMING
	t.toc();
#endif
}

MaxPool::MaxPool(int rows_in, int cols_in, int depth_in, int size, int stride){
	this->rows_in = rows_in;
	this->cols_in = cols_in;
	this->depth_in = depth_in;
	this->rows_out = (rows_in - size + 1) / stride + 1;
	this->cols_out = (rows_in - size + 1) / stride + 1;
	this->depth_out = depth_in;
	this->size = size;
	this->stride = stride;
}

void MaxPool::forward(Tensor& Xin, Tensor& Xout){	
	Xout.create(Xin.getNoInstances(), Xin.getDepth(), rows_out, cols_out);
	Maxi = Xout;
	Xin.tensorize();
	for (int i = 0; i < Xin.getNoInstances(); i++){
		for (int d = 0; d < Xin.getDepth(); d++){
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){

					int umax = 0, vmax = 0;
					T maxval = Xin[i][d](row*stride, col*stride);
					for (int u = 0; u < size; u++){
						for (int v = 0; v < size; v++){
							if (Xin[i][d](row*stride + u, col*stride + v) > maxval){
								maxval = Xin[i][d](row*stride + u, col*stride + v);
								umax = u; 
								vmax = v;
							}
						}
					}

					Maxi[i][d](row, col) = umax*size + vmax;
					Xout[i][d](row, col) = maxval;
				}
			}
		}
	}
}

void MaxPool::backward(Tensor& dXin, Tensor& dXout){
	dXin.create(dXout.getNoInstances(), depth_in, rows_in, cols_in);
	dXout.tensorize();
	for (int i = 0; i < dXin.getNoInstances(); i++){
		for (int d = 0; d < depth_in; d++){
			dXin[i][d].setTo(0);
			for (int row = 0; row < rows_out; row++){
				for (int col = 0; col < cols_out; col++){
					int umax = (int)Maxi[i][d](row, col) / size;
					int vmax = (int)Maxi[i][d](row, col) % size;
					dXin[i][d](row*stride + umax, col*stride + vmax) = dXout[i][d](row, col);
				}
			}
		}
	}
}

Dropout::Dropout(T prob){
	this->drop_prob = prob;
	this->drop_prob_stash = prob;
	this->iprob = 1.0 / (1-prob);
}

void Dropout::forward(Tensor& Xin, Tensor& Xout){
	std::default_random_engine dre;
	std::uniform_real_distribution<T> dist(0, 1);	
#if FIXED_SEED
	dre.seed(FIXED_SEED);
#else
#endif
	dre.seed(clock());	
	mask.createLike(Xin);
	Xout.createLike(Xin);
	if (Xin.getCurrentForm() == TENSOR_FORM){
		for (int i = 0; i < Xin.getNoInstances(); i++){
			for (int d = 0; d < Xin.getDepth(); d++){
				for (int row = 0; row < Xin.getHeight(); row++){
					for (int col = 0; col < Xin.getWidth(); col++){
						T x = dist(dre);
						if (x < drop_prob){
							mask[i][d](row, col) = 0;
							Xout[i][d](row, col) = 0;
						}
						else{
							mask[i][d](row, col) = 1;
							Xout[i][d](row, col) = Xin[i][d](row, col) * iprob;							
						}
					}
				}
			}
		}
	}
	else{
		for (int i = 0; i < Xin.getNoInstances(); i++){
			for (int j = 0; j < Xin.getDim(); j++){
				T x = dist(dre);
				if (x < drop_prob){
					mask(i, j) = 0;
					Xout(i, j) = 0;
				}
				else{
					mask(i, j) = 1;
					Xout(i, j) = Xin(i, j) * iprob;
				}
			}
		}
	}
}

void Dropout::backward(Tensor& dXin, Tensor& dXout){	
	dXin.createLike(dXout);
	if (dXout.getCurrentForm() == TENSOR_FORM){
		for (int i = 0; i < Xin.getNoInstances(); i++){
			for (int d = 0; d < Xin.getDepth(); d++){
				for (int row = 0; row < Xin.getHeight(); row++){
					for (int col = 0; col < Xin.getWidth(); col++){
						if (mask[i][d](row, col))
							dXin[i][d](row, col) = 0;							
						else
							dXin[i][d](row, col) = dXout[i][d](row, col);							
					}
				}
			}
		}
	}
	else{
		for (int i = 0; i < dXin.getNoInstances(); i++){
			for (int j = 0; j < dXin.getDim(); j++){
				if (mask(i, j))
					dXin(i, j) = dXout(i, j);
				else
					dXin(i, j) = 0;
			}
		}
	}
}

void Dropout :: trainingMode(){
	drop_prob = drop_prob_stash;
}

void Dropout::testMode(){
	drop_prob = 0;
}


