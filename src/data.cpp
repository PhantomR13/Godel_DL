#include "common.h"
#include "data.h"

#define MAX_INST_DATA (10000)

Data::Data(){
	no_classes = 0;
	batch_size = 0;
	batch_poz = 0;
}

void Data::split(Mat img, Tensor& X, int row){
	if (img.type() == CV_8UC1){
		if (sizeof(T) == 4)
			img.convertTo(img, CV_32FC1);
		else if (sizeof(T) == 8)
			img.convertTo(img, CV_64FC1);
		img = img / 255.0;// -0.5;
		X[row][0] = img;
	}
	else if (img.type() == CV_8UC3){

	}
	else{
		throw new Exception();
	}
}

void Data::loadImageDataset(char* folder){
	FileGetter fg(folder);
	vector<string> class_names;
	char buff[256];
	int max_instances = MAX_INST_DATA;

	//parse once to create summary
	int no_inst = 0;
	int no_features = 0;
	Mat img;
	while (fg.getNextFile(buff)){
		class_names.push_back(buff);
		char subfolder[256];
		sprintf(subfolder, "%s/%s", folder, buff);
		FileGetter fg2(subfolder);
		char img_name[256];
		int crt = 0;
		while (fg2.getNextAbsFile(img_name) && crt < max_instances){
			if (no_features == 0){
				img = imread(img_name, CV_LOAD_IMAGE_UNCHANGED);								
				no_features = img.rows * img.cols * img.channels();
			}
			crt++;
			no_inst++;
		}
	}
	printf("Directory contains %d classes with %d total number of instances, %d features each\n", class_names.size(), no_inst, no_features);
	printf("NOTE: Number of instances per class is limited to: %d.\n", max_instances);

	X = Tensor(no_inst, img.channels(), img.rows, img.cols);
	no_classes = (int)class_names.size();
	y = Mat_<T>(no_inst, 1);

	//parse again and read data
	fg = FileGetter(folder);
	int row = 0;
	int percent = 0;
	T classi = 0;
	while (fg.getNextFile(buff)){
		char subfolder[256];
		sprintf(subfolder, "%s/%s", folder, buff);
		FileGetter fg2(subfolder);
		char img_name[256];
		int crt = 0;
		while (fg2.getNextAbsFile(img_name) && crt < max_instances){
			img = imread(img_name, CV_LOAD_IMAGE_UNCHANGED);
			split(img, X, row);
			y(row) = classi;			
			row++;
			if (row * 100 / no_inst > percent){
				percent++;
				printf("\r%d of %d loaded %d %% ", row, no_inst, percent);
			}
			crt++;
		}
		classi++;
	}
	//normalizeData();
	printf("Done\n");
}

void Data::loadFromFile(char* fname){
	FILE* f = fopen(fname, "r");
	if (!f) throw exception("Data::loadFromFile - Inexistent file");
	int n, m;
	fscanf(f, "%d%d", &n, &m);	
#if 0
	//Tensor form - slower
	X.setNoInstances(n);
	y = Mat_<T>(n, 1);
	T lbl, x;
	for (int i = 0; i < n; i++){
		fscanf(f, "%f", &lbl);
		y(i) = lbl;
		for (int j = 0; j < m; j++){
			fscanf(f, "%f", &x);			
			Mat_<T> tmpmat; tmpmat.push_back(x);
			X[i].push_back(tmpmat);			
		}
	}
#else
	//Matrix form
	if (sizeof(T) != 8)
		throw exception("Data::loadFromFile sizeof(T)!=8");
	throw exception("Data::loadFromFile - not implemented");
	X = Tensor(n, m, 1, 1);
	Mat_<T> X_lin(n, m);
	y = Mat_<T>(n, 1);
	double lbl, x;
	no_classes = 0;
	for (int i = 0; i < n; i++){
		fscanf(f, "%lf", &lbl);
		y(i) = (T)lbl;
		no_classes = max(no_classes, (int)lbl);
		for (int j = 0; j < m; j++){
			fscanf(f, "%lf", &x);
			X_lin(i, j) = (T)x;
		}
	}
	no_classes++;
	X = X_lin;
#endif
	fclose(f);
}

void Data::normalizeData(){
	Mat_<T> Xlin = X.linearize();
	if (Xlin.rows == 0) 
		return;

	Mat_<T> mus(1, Xlin.cols);
	mus.setTo(0);
	Mat_<T> maxs(1, Xlin.cols);
	maxs.setTo(0);

	for (int i = 0; i < Xlin.rows; i++){
		for (int j = 0; j < Xlin.cols; j++){
			mus(0, j) += Xlin(i, j);			
		}
	}
	for (int j = 0; j < Xlin.cols; j++)
		mus(0, j) /= Xlin.rows;

	for (int i = 0; i < Xlin.rows; i++){
		for (int j = 0; j < Xlin.cols; j++){			
				Xlin(i, j) -= mus(0, j);
				maxs(0, j) = max(maxs(0, j), abs(Xlin(i, j)));
		}
	}

	for (int i = 0; i < Xlin.rows; i++){
		for (int j = 0; j < Xlin.cols; j++){
			if (maxs(0, j))
				Xlin(i, j) /= maxs(0, j);
		}
	}
}

void Data::createBatches(int batch_size, int perform_shuffle){
	this->batch_size = batch_size;
	batch_poz = 0;
	//if (batch_v.size()>0) return;
	batch_v.resize(getNoInstances());
	for (int i = 0; i < getNoInstances(); i++)
		batch_v[i] = i;
	if (perform_shuffle)
		random_shuffle(batch_v.begin(), batch_v.end());	
}

Data Data::nextBatch(){
	Data batch;
	batch.no_classes = no_classes;	
	if (batch_poz >= X.getNoInstances()){
		createBatches(batch_size);
	}
	int crn_batch_size = min(batch_size, X.getNoInstances() - batch_poz);
	Mat_<T> batch_y(crn_batch_size, 1);
	if (X.getCurrentForm() == MATRIX_FORM){
		throw std::exception("Data::nextBatch for matrix form not implemented");
		Mat_<T> X_lin = X.linearize();
	//	batch.X.create(crn_batch_size, X_lin.cols);
		Mat_<T> batch_lin(crn_batch_size, X_lin.cols);
		for (int i = 0; i < crn_batch_size; i++){
			//batch_lin.row(i) = X_lin.row(batch_v[batch_poz+i]);
			int i2 = batch_v[batch_poz + i];
			for (int j = 0; j < X_lin.cols; j++)
				batch_lin(i, j) = X_lin(i2, j);
			batch_y(i) = y(i2);
		}
		batch.X = batch_lin;		
	}
	else{
		batch.X.create(crn_batch_size, X.getDepth(), X.getHeight(), X.getWidth());
		for (int i = 0; i < crn_batch_size; i++){
			int i2 = batch_v[batch_poz + i];
			for (int j = 0; j < X.getDepth(); j++)
				//batch.X[i][j] = X[i2][j].clone();
				batch.X[i][j] = X[i2][j];
			batch_y(i) = y(i2);
		}
	}
	batch.y = batch_y;
	batch_poz += crn_batch_size;
	
	return batch;
}