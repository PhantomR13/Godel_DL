#pragma once
#include "common.h"
#include "tensor.h"

class OutLayer{
public:
	Tensor Xin;
	virtual void forward(Tensor& Xin, Mat_<T>& Target, T& out) = 0;
	virtual void backward(Tensor& dXin, Mat_<T>& Target, T dout) = 0;
};

class MeanSquaredError : public OutLayer{
public:
	void forward(Tensor& Xin, Mat_<T>& Target, T& out);
	void backward(Tensor& dXin, Mat_<T>& Target, T dout);
};

class SVMloss : public OutLayer{
public:
	void forward(Tensor& Xin, Mat_<T>& Target, T& out);
	void backward(Tensor& dXin, Mat_<T>& Target, T dout);
};

class CrossEntropy : public OutLayer{
public:
	void forward(Tensor& Xin, Mat_<T>& Target, T& out);
	void backward(Tensor& dXin, Mat_<T>& Target, T dout);
};

class SMCE : public OutLayer{
public:
	void forward(Tensor& Xin, Mat_<T>& Target, T& out);
	void backward(Tensor& dXin, Mat_<T>& Target, T dout);
};

class SVM : public OutLayer{
public:
	void forward(Tensor& Xin, Mat_<T>& Target, T& out);
	void backward(Tensor& dXin, Mat_<T>& Target, T dout);
};