#include "outlayer.h"

void MeanSquaredError::forward(Tensor& Xin, Mat_<T>& Target, T& out){
	this->Xin = Xin;
	Mat_<T> Xin_lin = Xin.linearize();
	out = 0;
	for (int i = 0; i < Target.rows; i++)
		out += (Xin_lin(i) - Target(i))*(Xin_lin(i) - Target(i));
	out /= (2*Target.rows);
}

void MeanSquaredError::backward(Tensor& dXin, Mat_<T>& Target, T dout){
	dXin = Xin;	
	Mat_<T> Xin_lin = Xin.linearize();	
	Mat_<T> dXin_lin = dXin.linearize();
	for (int i = 0; i < Target.rows; i++)
		dXin_lin(i) = (Xin_lin(i) - Target(i));
}

void CrossEntropy::forward(Tensor& Xin, Mat_<T>& Target, T& out){
	this->Xin = Xin;
	Mat_<T> Xin_lin = Xin.linearize();
	out = 0;
	for (int i = 0; i < Target.rows; i++)
		out -= log(Xin_lin(i, (int)Target(i)));
	out /= Target.rows;
}

void CrossEntropy::backward(Tensor& dXin, Mat_<T>& Target, T dout){
	Mat_<T> Xin_lin = Xin.linearize();
	dXin = Xin;
	Mat_<T> dXin_lin = dXin.linearize();
	for (int i = 0; i < dXin_lin.rows; i++){
		for (int j = 0; j < dXin_lin.cols; j++){
			if (j == Target(i))
				dXin_lin(i, j) = -1 / Xin_lin(i, j);
			else
				dXin_lin(i, j) = 0;
		}
	}
}

void SMCE::forward(Tensor& Xin, Mat_<T>& Target, T& out){
	this->Xin = Xin;
	Mat_<T> Xin_lin = Xin.linearize();
	out = 0;
	for (int i = 0; i < Target.rows; i++){
		T mv = Xin_lin(i, 0);
		for (int j = 1; j < Xin_lin.cols; j++)
			mv = max(mv, Xin_lin(i, j));
		T sum = 0;
		for (int j = 0; j < Xin_lin.cols; j++)
			sum += exp(Xin_lin(i, j) - mv);
		out += log(sum) - Xin_lin(i, (int)Target(i)) + mv;
	}
	out /= Target.rows;
}

void SMCE::backward(Tensor& dXin, Mat_<T>& Target, T dout){
	dXin = Xin;
	Mat_<T> dXin_lin = dXin.linearize();
	Mat_<T> Xin_lin = Xin.linearize();
	for (int i = 0; i < Target.rows; i++){
		T mv = Xin_lin(i, 0);
		for (int j = 1; j < Xin_lin.cols; j++)
			mv = max(mv, Xin_lin(i, j));

		T sum = 0;
		for (int j = 0; j < Xin_lin.cols; j++){
			dXin_lin(i, j) = exp(Xin_lin(i, j) - mv);
			sum += dXin_lin(i, j);
		}

		if (sum){
			for (int j = 0; j < Xin_lin.cols; j++)	
				dXin_lin(i, j) /= sum;
		}
		dXin_lin(i, (int)Target(i)) -= 1;		
	}	
	dXin_lin /= Target.rows;
}

void SVM::forward(Tensor& Xin, Mat_<T>& Target, T& out){
	this->Xin = Xin;
	Mat_<T> Xin_lin = Xin.linearize();
	out = 0;
	for (int i = 0; i < Target.rows; i++){
		for (int j = 0; j < Xin_lin.cols; j++){
			out += max(0, 1 + Xin_lin(i, j) - Xin_lin(i, (int)Target(i)));
		}
		out -= 1;
	}
	//Aux = max(Xin - X_indY + 1, 0)
	//out = sum(Aux(:))/Target.rows
	out /= Target.rows;
}

void SVM::backward(Tensor& dXin, Mat_<T>& Target, T dout){
	
	dXin = Xin;
	Mat_<T> dXin_lin = dXin.linearize();
	dXin_lin.setTo(0);
	Mat_<T> Xin_lin = Xin.linearize();
	for (int i = 0; i < Target.rows; i++){
		for (int j = 0; j < Xin_lin.cols; j++){
			if (1 + Xin_lin(i, j) - Xin_lin(i, (int)Target(i)) <= 0)
				dXin_lin(i, j) = 0;
			else{
				dXin_lin(i, (int)Target(i)) -= 1;			
				dXin_lin(i, j) += 1;
			}
		}
	}
	dXin_lin /= Target.rows;
	//Aux = max(Xin-X_Indy+1,0)
	//dXin = Aux>0 + sum(Aux,2)*Indy	
}
