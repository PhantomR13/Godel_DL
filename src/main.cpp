#include "opencv2\opencv.hpp"
#include "data.h"
#include "tensor.h"
#include "network.h"
#include "solver.h"
#include "evaluation.h"
#include "main.h"
#include "test.h"
using namespace cv;

void my_network()
{
	char* TRAINING_SET_PATH	= "..\\data\\3CLASSES_belgium_ts\\Training";
	char* TESTING_SET_PATH	= "..\\data\\3CLASSES_belgium_ts\\Testing";
	
	Params params, outparams;
	params.set("learning_rate", 1e-3);
	params.set("max_epochs", 100);
	params.set("learning_rate_decay", 9e-1);
	params.set("learning_rate_epochs", 10);
	params.set("batch_size", 100);	
	params.set("regularization_factor", 1e-2);

	//params.set("target_loss", 1e-7);
	//params.set("max_iter", 500);


	Data trainingData;
	trainingData.loadImageDataset(TRAINING_SET_PATH);

	Network nn(&params);
	nn.add(new Convolution(1, 28, 5, 32));//0
	nn.add(new ReLU());//1
	nn.add(new MaxPool(28, 28, 32, 2, 2));

	nn.add(new FullLayer(14 * 14 * 32, 256));//6
	nn.add(new ReLU());

	nn.add(new FullLayer(256, 62));
	//nn.add(new ReLU());//7
	nn.outlayer = new SMCE();

	SGDSolver solver(&params);
	Timer t;
	T end_loss = solver.learn(&trainingData, &nn);
	outparams.set("final loss", end_loss);
	outparams.set("training time", t.toc());

	//((Dropout*)nn.layers[8])->testMode();
	Evaluation eval;
	outparams.set("training error", eval.classification_error(&trainingData, &nn));

	Data testingData;
	testingData.loadImageDataset(TESTING_SET_PATH);
	outparams.set("test error", eval.classification_error(&testingData, &nn));

	printf("SUCCESS!\n");
}

int main(){
	my_network();

	getchar();
  return 0;
}
