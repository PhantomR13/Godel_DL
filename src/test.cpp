#include "opencv2\opencv.hpp"
#include "data.h"
#include "tensor.h"
#include "network.h"
#include "solver.h"
#include "evaluation.h"
using namespace cv;

void fix_dataset(char *path)
{
	FileGetter fg(path);
	char buff[256];

	//parse once to create summary
	int no_inst = 0;
	int no_features = 0;
	Mat img;
	Mat dst;
	while (fg.getNextFile(buff)) {
		char subfolder[256];
		sprintf(subfolder, "%s/%s", path, buff);
		FileGetter fg2(subfolder);
		char img_name[256];
		int crt = 0;
		while (fg2.getNextAbsFile(img_name)) {
			img = imread(img_name, CV_LOAD_IMAGE_GRAYSCALE);
			resize(img, dst, Size(28, 28));
			if (img.empty())
			{
				throw new exception("Image could not be opened!\n");
			}
			remove(img_name);
			img_name[strlen(img_name) - 2] = 'g';
			imwrite(img_name, dst);
		}
	}
}


void test_regression(){ 
	Params params;
	params.set("learning_rate", 1e-1);
	params.set("target_loss", 1e-7);
	params.set("max_iter", 1000);

	Data data;
	data.loadFromFile("..\\data\\regression0.txt");
	
	Network nn(&params);
	nn.add(new FullLayer(data.getDim(), 1));
	nn.outlayer = new MeanSquaredError();

	GDSolver solver(&params);
	solver.learn(&data, &nn);
	cout << ((FullLayer*)nn.layers[0])->W << endl;
}

void test_logistic_regression(){
	Params params, outparams;
	params.set("learning_rate", 1e-2);
	params.set("target_loss", 1e-7);
	params.set("max_iter", 500);
	params.set("batch_size", 50);
	params.set("regularization_factor", 1e-3);

	Data data;
	data.loadFromFile("..\\data\\logistic_regression2.txt");

	Network nn(&params);
	nn.add(new FullLayer(data.getDim(), data.getNoClasses()));
	//nn.outlayer = new SMCE();
	nn.outlayer = new SVM();

	GDSolver solver(&params);
	solver.learn(&data, &nn);
	//cout << ((FullLayer*)nn.layers[0])->W << endl;

	Evaluation eval;
	outparams.set("training error", eval.classification_error(&data, &nn));
}

void test_1layer_MNIST(){
	Params params;
	params.set("learning_rate", 1e-2);
	params.set("target_loss", 1e-7);
	params.set("max_iter", 200);
	params.set("batch_size", 1000);
	params.set("regularization_factor", 1e-3);

	Params outparams;

	Data data;
	data.loadImageDataset("d:/imgdb/MNIST/train");

	Network nn(&params);
	nn.add(new FullLayer(data.getDim(), data.getNoClasses()));
	nn.outlayer = new SMCE();
	//nn.outlayer = new SVM();

	GDSolver solver(&params);
	Timer t;
	T end_loss = solver.learn(&data, &nn);
	outparams.set("final loss", end_loss);
	outparams.set("training time", t.toc());

	Evaluation eval;
	outparams.set("training error", eval.classification_error(&data, &nn));

	Data test_data;
	test_data.loadImageDataset("d:/imgdb/MNIST/test");
	outparams.set("test error", eval.classification_error(&test_data, &nn));

	//nn.saveToFile("../data/models/logistic_regression3.txt", &outparams);
}

void test_2layer_MNIST(){
	Params params;
	params.set("learning_rate", 1e-5);
	params.set("target_loss", 1e-7);
	params.set("max_iter", 2000);
	int hidden_layer_size = 100;
	int hidden_layer_size2 = 100;
	params.set("hidden1", hidden_layer_size);
	params.set("hidden2", hidden_layer_size2);
	//params.set("batch_size", 600);
	
	Params outparams;

	Data data;
	data.loadImageDataset("d:/imgdb/MNIST/train");
	//data.loadFromFile("../data/xor.txt");

	Network nn(&params);

	nn.add(new FullLayer(data.getDim(), hidden_layer_size));
	nn.add(new ReLU());
	nn.add(new FullLayer(hidden_layer_size, hidden_layer_size2));
	nn.add(new ReLU());
	nn.add(new FullLayer(hidden_layer_size2, data.getNoClasses()));
	nn.outlayer = new SMCE();
	//nn.outlayer = new SVM();

	GDSolver solver(&params);
	Timer t;
	T end_loss = solver.learn(&data, &nn);
	outparams.set("final loss", end_loss);
	outparams.set("training time", t.toc());

	Evaluation eval;
	outparams.set("training error", eval.classification_error(&data, &nn));

	Data test_data;
	test_data.loadImageDataset("d:/imgdb/MNIST/test");
	outparams.set("test error", eval.classification_error(&test_data, &nn));

	//nn.saveToFile("../data/models/full_relu_full_relu_full.txt", &outparams);
}

void test_2layer_conv_MNIST(){
	Params params;
	params.set("learning_rate", 1e-1);
	params.set("target_loss", 1e-7);
	params.set("max_epochs", 200);
	params.set("learning_rate_decay", 9e-1);
	params.set("learning_rate_epochs", 10);
	params.set("batch_size", 1000);
	params.set("regularization_factor", 1e-3);

	Params outparams;

	Params conv1_params;
	int depth_out = 4;
	conv1_params.set("w", 3);
	conv1_params.set("depth_in", 1);
	conv1_params.set("rows_in", 28); 
	conv1_params.set("cols_in", 28);
	conv1_params.set("depth_out", depth_out);
	conv1_params.set("pad", 1);
	conv1_params.set("stride", 1);

	Data data;
	data.loadImageDataset("d:/imgdb/MNIST/train");
	//data.loadFromFile("../data/xor.txt");

	Network nn(&params);

	nn.add(new Convolution(&conv1_params));
	nn.add(new ReLU());
	//nn.add(new Dropout(0.4));
	nn.add(new FullLayer(28*28*depth_out, 10)); //drmem
	nn.outlayer = new SMCE();
	//nn.outlayer = new SVM();

	SGDSolver solver(&params);
	Timer t;
	T end_loss = solver.learn(&data, &nn);
	outparams.set("final loss", end_loss);
	outparams.set("training time", t.toc());	
	//nn.layers.erase(nn.layers.begin() + 2);

	Evaluation eval;
	outparams.set("training error", eval.classification_error(&data, &nn));

	Data test_data;
	test_data.loadImageDataset("d:/imgdb/MNIST/test");
	outparams.set("test error", eval.classification_error(&test_data, &nn));

	//nn.saveToFile("../data/models/conv_relu_full.txt", &outparams);
}

void conv_tests(){
	Data data;
	data.loadImageDataset("d:/imgdb/MNIST/train");

	Params conv1_params;
	int depth_out = 1;
	int w = 3;
	conv1_params.set("w", w);
	conv1_params.set("depth_in", 1);
	conv1_params.set("rows_in", 28);
	conv1_params.set("cols_in", 28);
	conv1_params.set("depth_out", depth_out);
	conv1_params.set("pad", w/2);
	conv1_params.set("stride", 1);
	Layer* l = new Convolution(&conv1_params);
	
	l->W.setTo(-1.f / 9);
	//l->W(w / 2 * w + w / 2) = -1.f/9;
	l->W(w*w) = 1;

	Tensor Xout;
	l->forward(data.X, Xout);

	T kdata[] = { 1,1,1,1,1,1,1,1,1 };
	Mat_<T> kk(3, 3, kdata);

	int ok = 1;
	for (int i = 0; i < data.X.getNoInstances(); i++){
		Mat_<T> res = Xout[i][0];
		Mat_<T> gt;
		filter2D(data.X[i][0], gt, CV_32FC1, kk, Point(-1, -1), 0, IPL_BORDER_CONSTANT); // data.X[i][0] * -1 + 1;
		gt = 1 - gt / 9;
		for (int ii = 0; ii < res.rows; ii++){
			for (int jj = 0; jj < res.cols; jj++){
				if (abs(gt(ii, jj) - res(ii, jj))>1e-4){
					T gtiijj = gt(ii, jj);
					T resiijj = res(ii, jj);
					ok = 0;
				}
			}
		}
		if (!ok){
			printf("problem at %d\n", i);
		}
	}
	printf("%d\n", ok);
	//waitKey();
 }

void multi_conv_MNIST(){
	Params params, outparams;
	params.set("learning_rate", 1e-3);
	params.set("max_epochs", 100);
	params.set("learning_rate_decay", 9e-1);
	params.set("learning_rate_epochs", 10);
	params.set("batch_size", 100);
	params.set("regularization_factor", 1e-2);

	Data data;
	data.loadImageDataset("d:/imgdb/MNIST/train");

	Network nn(&params);
	nn.add(new Convolution(1, 28, 3, 32));//0
	nn.add(new ReLU());//1
	nn.add(new MaxPool(28, 28, 32, 2, 2));//2

	nn.add(new Convolution(32, 14, 3, 64));//3
	nn.add(new ReLU());//4
	nn.add(new MaxPool(14, 14, 64, 2, 2));//5

	nn.add(new FullLayer(64*7*7, 1024));//6
	nn.add(new ReLU());//7
	//nn.add(new Dropout(0.5));//8
	nn.add(new FullLayer(1024, 10));//9
	nn.outlayer = new SMCE();

	SGDSolver solver(&params);
	Timer t;
	T end_loss = solver.learn(&data, &nn);
	outparams.set("final loss", end_loss);
	outparams.set("training time", t.toc());

	//((Dropout*)nn.layers[8])->testMode();
	Evaluation eval;
	outparams.set("training error", eval.classification_error(&data, &nn));

	Data test_data;
	test_data.loadImageDataset("d:/imgdb/MNIST/test");
	outparams.set("test error", eval.classification_error(&test_data, &nn));

	nn.saveToFile("../data/models/conv_relu_max2_full_dropout_full.txt", &outparams);
}