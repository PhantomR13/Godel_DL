#pragma once
#include "common.h"
#include "data.h"
#include "network.h"
#include <random>

class Evaluation{
public:
	T classification_error(Data* data, Network* nn);
	void crossvalidation(Data* data, int nrfolds);
	void randomsplit(Data* data, float train_percent);
	void showcase(Data* data, Network* nn);
};