#pragma once
#include "common.h"
#include "data.h"

class Classifier{
public:
	Mat_<T> predict(Mat_<T> X);
	void train(Data data);
};