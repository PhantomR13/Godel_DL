All images of the Belgium TS dataset -> grayscale, 28x28

Network:
- conv layer, depth =32
- ReLU
- MaxPool 2x2, stride = 2
- 256 outputs full layer
- ReLU
- 62 outputs full layer
- output: SMCE